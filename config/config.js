module.exports={
    // 小程序信息
    appInfo: {
        AppID: "",
        AppSecret: "",
    },
    redis: {
        host: "127.0.0.1",
        port: 6379,
        pass: "" //没有就不填
    },
    site: {
        url: "", //网站地址，结尾无斜杠
        key: "", //api KEY
        author: "Chrise" //文章作者名(halo文章返回数据中好像没有)
    },
    subscribe: {
        comment: "", // 留言审核模板id
        article: "", //新文章订阅模板id
    }
}