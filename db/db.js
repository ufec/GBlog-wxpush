const mongoose = require('mongoose');
const dbConf = require('../config/database');
var log4js = require('../utils/log');
var log = log4js.getLogger('mongoose');

const DB_URL = `mongodb://${dbConf.address}:${dbConf.port}/${dbConf.database}`;

mongoose.connect(DB_URL, { useNewUrlParser: true }, (err) => {
	log.error(err);
});


mongoose.connection.on('connected', () => {
	console.log('数据库连接成功')
})

mongoose.connection.on('disconnected', () => {
	console.log('数据库断开')
})

mongoose.connection.on('error', () => {
	console.log('数据库连接异常')
})

// 将此文件作为一个模块 暴露出去，供别人调用
module.exports = mongoose;