const mongoose = require('../db'); // 引入数据库连接模块
const Schema = mongoose.Schema; // 拿到当前数据库相应的集合对象

// 设计评论表的集合
const commentSchema = new Schema({
	//用户标识
	openId: { 
		type: String,
		trim: true,
	},

	//用户昵称
	author: { 
		type: String,
		trim: true, 
	},

	articleId: { 
		type: Number,
		trim: true, 
	},//文章id

	//评论id(唯一识别)
	commentId: { 
		type: Number,
		trim: true, 
	},

	// 文章标题
	title: { 
		type: String,
		trim: true, 
	}, 

	// 评论内容
	content: { 
		type: String,
		trim: true, 
	}, 

	//订阅次数
	subscribeTimes: { 
		type: Number,
		trim: true, 
	},
})

var Comment = mongoose.model('Comment', commentSchema);
module.exports = Comment