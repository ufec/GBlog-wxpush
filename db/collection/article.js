const mongoose = require('../db'); // 引入数据库连接模块
const Schema = mongoose.Schema; // 拿到当前数据库相应的集合对象

// 设计评论表的集合
const articleSchema = new Schema({
	openId: { 
		type: String,
		trim: true,
	},//用户标识
	subscribeTimes: { 
		type: Number,
		trim: true, 
	},//订阅次数
})

var Article = mongoose.model('Article', articleSchema);
module.exports = Article