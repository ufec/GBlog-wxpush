# GBlog-wxpush

#### 介绍
用于GBlog-wx小程序项目的微信推送服务

### 一、安装
```
git clone https://gitee.com/ufec/GBlog-wxpush.git
npm install
```

### 二、配置
参照下方目录结构中的config说明来配置，其中小程序两个模板的要求如下图：
![文章推送](https://ae01.alicdn.com/kf/H1a362fa39d3743ed933bad9e23596ddd4.jpg)

![留言推送](https://ae01.alicdn.com/kf/Haa4e7a3fa1f24a178edb0c25a51b4f3b0.jpg)

#### !注意：推送模板已经写死，如要更改可见 /utils/schedule.js，但不建议更改，按照上述图片配置即可
如果你想更改halo博客的微信分享功能([寒山主题](https://github.com/halo-dev/halo-theme-hshan)),如下图所示：
![微信分享](https://ae01.alicdn.com/kf/H06d3485cfc064408b3d04ce82e8de93eH.jpg)
可前往[Ufec寻求帮助](https://www.ufec.cn)，我只做了寒山主题的修改，其他主题自行处理

### 三、运行
```
npm start                //默认端口5000，可在package.json文件中修改
node /utils/schedule.js  //启动定时器程序
```
### 四、说明
此项目的小程序端是由[fuzui](https://gitee.com/fuzui)开源的[GBlog-wx](https://gitee.com/GeekEra/GBlog-wx)构成，依托于[Halo博客](https://halo.run)

### 四、目录结构
```
├── app.js                   //主程序
├── config                   //配置目录
│   ├── config.js            //主要配置
│   ├── database.js          //数据库配置
│   └── log4js.json          //日志配置
├── db                       //数据库目录
│   ├── collection           //数据模型
│   │   ├── article.js       //文章模型
│   │   └── comment.js       //评论模型
│   └── db.js                //数据库文件
├── log                      //日志目录
├── package.json             
├── routes                   //路由文件目录
│   ├── article.js           //文章路由
│   ├── comment.js           //评论路由
│   ├── getOpenId.js         //获取用户openid路由
│   └── getQRCode.js         //生成小程序码路由
└── utils                    //功能
    ├── getToken.js          //获取微信AccessToken
    ├── log.js               //初始化日志配置
    ├── redis.js             //redis
    ├── schedule.js          //定时器
    ├── sendMsg.js           //下发模板信息
```