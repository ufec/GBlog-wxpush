const express =  require('express');
const router = express.Router();
const comment = require('../db/collection/comment')

router.post('/', (req, res, next)=>{
    var data = req.body;
    if (!('openId' in data) || !('articleId' in data) || !('commentId' in data) || !('author' in data)) {
        res.send({
            code: 400,
            msg: "异常"
        });
    }else{
        // 评论id唯一，只可新增
        comment.insertMany(data).then(ret=>{
            if (ret) {
                res.send({
                    code: 200,
                    msg: "成功"
                })
            }
        })
    }
})


module.exports = router;