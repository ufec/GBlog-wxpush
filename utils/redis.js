const redis = require('redis')	//引用redis模块
const redisConf = require('../config/config').redis
const client = redis.createClient({host: redisConf.host, port: redisConf.port});
var log = require('./log');

if (redisConf.pass) {
    client.auth(redisConf.pass, (err,reply)=>{
        log.getLogger('redis').error(err)
    })
}

//连接失败回调
client.on('error',function(err){
    console.log("Redis连接失败:",err);
});

class Cache{
	//创建set方法
	//@key = 属性名
	//@value = 属性值
	//@expire = 过期时间（秒计
    set(key,value,expire){
    	//这里就是promise回调
        return new Promise(function(callback,errback){
        	//新建一个键值对保存到redis
            client.set(key,value,function(err,result){
                if(err){
                	//保存失败
                    console.log(err)
                    return
                }
                //如果expire存在的话，再设置一下这个键值对的过期时间
                if (!isNaN(expire) && expire > 0) {
                    client.expire(key, parseInt(expire));
                }
                //回调
                callback(result)
            })
        })
    }
    //取出一个之前存储过的键值对
    //@key = 属性名，也就是键值对的键名
    get(key){
        return new Promise(function(callback,errback){
            client.get(key,function(err,result){
                if(err) return
                callback(result)
            })
        })
    }
}
//输出Cache类
module.exports = Cache