var token = require('./getToken');
const request = require('request');

function sendMsg(postData)
{
    return token.AccessToken().then(res=>{
        let AccessToken = res;
        let api = `https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=${AccessToken}`;
        return new Promise((resolve, reject) => {
            request({
                url: api,
                method: 'POST',
                body: JSON.stringify(postData),
                headers: {
                    'Content-Type' : 'application/json'
                }
            }, (err, response, body)=>{
                let data = JSON.parse(body);
                if (data.errcode != 0) {
                    reject(data);
                }else{
                    resolve('OK');
                }
            });
        });
    }, err=>{
        return null;
    })
}
module.exports = sendMsg;