var log4js = require('log4js');
// 以node启动
// log4js.configure('./config/log4js.json');

// // pm2项目管理器启动
// log4js.configure('../config/log4js.json');

// 通用版
log4js.configure({
	"appenders": {
		"access": {
			"type": "dateFile",
			"filename": "log/access",
			"pattern": "yyyy-MM-dd.log",
			"alwaysIncludePattern": true,
			"category": "http"
		},
		"app": {
			"type": "file",
			"filename": "log/app.log",
			"maxLogSize": 10485760,
			"numBackups": 3
		},
		"errorFile": {
			"type": "file",
			"filename": "log/errors.log"
		},
		"errors": {
			"type": "logLevelFilter",
			"level": "ERROR",
			"appender": "errorFile"
		}
	},
	"categories": {
		"default": {
			"appenders": [
				"app",
				"errors"
			],
			"level": "DEBUG"
		},
		"http": {
			"appenders": [
				"access"
			],
			"level": "DEBUG"
		},
		"mongoose": {
			"appenders": [
				"errors"
			],
			"level": "ERROR"
		},
		"redis": {
			"appenders": [
				"errors"
			],
			"level": "ERROR"
		}
	}
});
exports.getLogger = function (name) {
    return log4js.getLogger(name || 'default')
};